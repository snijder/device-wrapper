# -*- coding: utf-8 -*-
#
# This file is part of the PCC project
#
#
#
# Distributed under the terms of the APACHE license.
# See LICENSE.txt for more info.

""" PCC Device Server for LOFAR2.0

"""

# PyTango imports
from tango.server import run
from tango.server import device_property
from tango import DevState
# Additional import

from clients.test_client import example_client
from src.attribute_wrapper import *
from src.hardware_device import *

__all__ = ["test_device", "main"]


class test_device(hardware_device):

	# -----------------
	# Device Properties
	# -----------------

	OPC_Server_Name = device_property(
		dtype='DevString',
	)

	OPC_Server_Port = device_property(
		dtype='DevULong',
	)

	OPC_Time_Out = device_property(
		dtype='DevDouble',
	)

	# ----------
	# Attributes
	# ----------
	bool_scalar_R = attribute_wrapper(comms_annotation="numpy.bool_ type read scalar", datatype=numpy.bool_)
	bool_scalar_RW = attribute_wrapper(comms_annotation="numpy.bool_ type read/write scalar", datatype=numpy.bool_, access=AttrWriteType.READ_WRITE)

	int64_spectrum_R = attribute_wrapper(comms_annotation="numpy.int64 type read spectrum (len = 8)", datatype=numpy.int64, dims=(8,))
	str_spectrum_RW = attribute_wrapper(comms_annotation="numpy.str type read/write spectrum (len = 8)", datatype=numpy.str_, dims=(8,), access=AttrWriteType.READ_WRITE)

	double_image_R = attribute_wrapper(comms_annotation="numpy.double type read image (dims = 2x8)", datatype=numpy.double, dims=(2, 8))
	double_image_RW = attribute_wrapper(comms_annotation="numpy.double type read/write image (dims = 8x2)", datatype=numpy.double, dims=(8, 2), access=AttrWriteType.READ_WRITE)

	int32_scalar_R = attribute_wrapper(comms_annotation="numpy.int32 type read scalar", datatype=numpy.int32)
	uint16_spectrum_RW = attribute_wrapper(comms_annotation="numpy.uint16 type read/write spectrum (len = 8)", datatype=numpy.uint16, dims=(8,), access=AttrWriteType.READ_WRITE)
	float32_image_R = attribute_wrapper(comms_annotation="numpy.float32 type read image (dims = 8x2)", datatype=numpy.float32, dims=(8, 2))
	uint8_image_RW = attribute_wrapper(comms_annotation="numpy.uint8 type read/write image (dims = 2x8)", datatype=numpy.uint8, dims=(2, 8), access=AttrWriteType.READ_WRITE)

	# --------
	# overloaded functions
	# --------
	def initialise(self):
		""" user code here. is called when the sate is set to INIT """
		"""Initialises the attributes and properties of the PCC."""

		self.set_state(DevState.INIT)


		#set up the OPC ua client
		self.example_client = example_client(self.Standby, self.Fault, self)

		# map an access helper class
		for i in self.attr_list():
			i.set_comm_client(self.example_client)


		self.example_client.start()

# ----------
# Run server
# ----------
def main(args=None, **kwargs):
	"""Main function of the example module."""
	return run((test_device,), args=args, **kwargs)


if __name__ == '__main__':
	main()
