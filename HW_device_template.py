# -*- coding: utf-8 -*-
#
# This file is part of the PCC project
#
#
#
# Distributed under the terms of the APACHE license.
# See LICENSE.txt for more info.

""" Hardware Device Server for LOFAR2.0

"""

# PyTango imports
from tango.server import run
# Additional import

from src.hardware_device import *


__all__ = ["HW_dev"]

class HW_dev(hardware_device):
	"""
	This class is the minimal (read empty) implementation of a class using 'hardware_device'
	"""

	# ----------
	# Attributes
	# ----------
	"""
	attribute wrapper objects can be declared here. All attribute wrapper objects will get automatically put in a ist (attr_list) for easy access
	
	example = attribute_wrapper(comms_annotation="this is an example", datatype=numpy.double, dims=(8, 2), access=AttrWriteType.READ_WRITE)
	...
	
	"""

	def always_executed_hook(self):
		"""Method always executed before any TANGO command is executed."""
		pass

	def delete_device(self):
		"""Hook to delete resources allocated in init_device.

		This method allows for any memory or other resources allocated in the
		init_device method to be released.  This method is called by the device
		destructor and by the device Init command (a Tango built-in).
		"""
		self.debug_stream("Shutting down...")

		self.Off()
		self.debug_stream("Shut down.  Good bye.")

	# --------
	# overloaded functions
	# --------
	def fault(self):
		""" user code here. is called when the state is set to FAULT """
		pass

	def off(self):
		""" user code here. is called when the state is set to OFF """
		pass

	def on(self):
		""" user code here. is called when the state is set to ON """

		pass

	def standby(self):
		""" user code here. is called when the state is set to STANDBY """
		pass

	def initialise(self):
		""" user code here. is called when the sate is set to INIT """
		pass

# ----------
# Run server
# ----------
def main(args=None, **kwargs):
	"""Main function of the hardware device module."""
	return run((HW_dev,), args=args, **kwargs)


if __name__ == '__main__':
	main()

