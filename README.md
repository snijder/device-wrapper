# Device wrapper

This code provides an attribute_wrapper class in place of attributes for tango devices. the attribute wrappers contain additional code 
that moves a lot of the complexity and redundant code to the background. 

The tango Device class is also abstracted further to a "hardware_device" class. This class wraps 

The only things required on the users part are to declare the attributes using the attribute_wrapper (see `example/example_device`),
declare what client the attribute has to use in the initialisation and provide support for the used clients. 
To see how to add support for new clients, see `clients/README.md`

In addition it also provides an abstraction to the tango device, specifically for hardware devices. Examples of hardware devices 
can be found in TODO and an empty template can be found in `HW_device_template.py`

Requires numpy 
```pip install numpy```

Requires opcua 
```pip install opcua```

Requires pytango 
```pip install pytango```

### usage
You can start the device by calling it in any console with:
<Device_name>.py instance_name
